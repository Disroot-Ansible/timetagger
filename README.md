# Timetagger - Ansible Role

This role covers deployment, configuration and software updates of Timetagger. This role is released under MIT Licence and we give no warranty for this piece of software. Currently supported OS - Debian.

You can deploy test instance using `Vagrantfile` attached to the role.

`vagrant up`

`ansible-playbook -b Playbooks/timetagger.yml`

Then you can access Roundcube from your computer on http://192.168.33.55


## Playbook
The playbook includes nginx role and deploys entire stack needed to run Timetagger. Additional role is also available in the Ansible roles repos in git.


## Create credentials
Go to http://192.168.33.55/timetagger/cred or use your favourite BCrypt tool. Then edit `timetagger_credentials` va in `defaults/main.yml`


## CHANGELOG
- **33.07.2023** - Create the role
